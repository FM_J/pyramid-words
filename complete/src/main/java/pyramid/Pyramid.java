package pyramid;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.stream.IntStream;

public class Pyramid {
    public static boolean isPyramidWord(String s){
        int length = s.trim().length();

        if(length < 1){
            return false;
        } else if(length == 1){
            return true;
        } else if(s.split(" ").length > 1){
            return false;
        }

        Hashtable<Character, Integer> charCount = new Hashtable<>();

        //if this is not a sum of consecutive numbers, then it cannot be a pyramid word
        int n = isConsecutiveSum(length);

        if(n == -1){
            return false;
        }

        int[] pyramidValues = IntStream.rangeClosed(1, n).toArray();

        for(char c : s.toCharArray()){
            charCount.compute(c, (k, v)->v==null? 1 : v+1);
        }

        ArrayList<Integer> countList = new ArrayList<>(charCount.values());
        for(int count : countList){
            if(pyramidValues[count-1] == 0){
                return false;
            } else{
                pyramidValues[count-1] = 0;
            }
        }

        return true;
    }

    static int isConsecutiveSum(int i){
        //input i is a sum of consecutive numbers if 1 -8i is a perfect square
        int x = 1 + (8 * i);

        double root = Math.sqrt(x);
        if((root - Math.floor(root)) == 0){
            return ((int)root - 1) / 2;
        }
        return -1;
    }
}
