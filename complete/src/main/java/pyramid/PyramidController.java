package pyramid;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PyramidController {
    @RequestMapping("/isPyramidWord")
    public boolean isPyramidWord(@RequestParam(value="string", defaultValue="") String string) {
        return Pyramid.isPyramidWord(string);
    }
}
