package pyramid;

import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;


class PyramidTest {


    @ParameterizedTest(name="Run {index}: input={0}, expectedResult={1}")
    @MethodSource("testIsPyramidWord")
    void isPyramidWord(String input, boolean expected) {
        boolean result = Pyramid.isPyramidWord(input);

        Assert.assertEquals("wrong result", expected, result);
    }

    static Stream<Arguments> testIsPyramidWord() throws Throwable {
        return Stream.of(
                Arguments.of("", false),
                Arguments.of("t", true),
                Arguments.of("q", true),
                Arguments.of("Q", true),
                Arguments.of("quit", false),
                Arguments.of("banana", true),
                Arguments.of("bandana", false),
                Arguments.of("bandaa", false),
                Arguments.of("abarberaba", true),
                Arguments.of("a b a r b eraba", false),
                Arguments.of("abarb eraba", false)
        );
    }

    @ParameterizedTest(name="Run {index}: input={0}, expectedResult={1}")
    @MethodSource("testIsConsecutiveSum")
    void isConsecutiveSum(int input, int expected) {
        int n = Pyramid.isConsecutiveSum(input);

        Assert.assertEquals("not correct return", expected, n);
    }

    static Stream<Arguments> testIsConsecutiveSum() throws Throwable {
        return Stream.of(
                Arguments.of(1, 1),
                Arguments.of(2, -1),
                Arguments.of(3, 2),
                Arguments.of(4, -1),
                Arguments.of(5, -1),
                Arguments.of(6, 3)
        );
    }
}