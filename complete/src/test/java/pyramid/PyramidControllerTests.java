/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pyramid;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.StreamingHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PyramidControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testIsPyramidWordDefaultReturnValue() throws Exception {

        this.mockMvc.perform(get("/isPyramidWord")).andExpect(status().isOk())
                .andExpect(content().string("false"));
    }

    @ParameterizedTest(name="Run {index}, inputString={0}, expectedResult={1}")
    @MethodSource("testIsPyramidWordController")
    public void testIsPyramidReturnValues(String input, boolean expected) throws Exception {

        this.mockMvc.perform(get("/isPyramidWord?string="+input)).andExpect(status().isOk())
                .andExpect(content().string(""+expected));
    }

    private static Stream<Arguments> testIsPyramidWordController() throws Throwable {
        return Stream.of(
                Arguments.of("", false),
                Arguments.of("t", true),
                Arguments.of("quit", false),
                Arguments.of("banana", true),
                Arguments.of("bandana", false)
        );
    }
}
